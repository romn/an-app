'use strict';

( function ( $ ) {
	var selectData;

	selectData = [
		{ id: 1, text: '1' },
		{ id: 2, text: '2' },
		{ id: 3, text: '3' },
		{ id: 4, text: '4' },
		{ id: 5, text: '5' },
		{ id: 6, text: '6' }
	];

	$( '[data-plugin-select2-id="001"]' ).select2();

	$( '[data-plugin-select2-id="002"]' ).select2( {
		data: selectData
	} );

	$( '[data-plugin-select2-id="003"]' ).select2( {
		ajax: {
			quietMillis: 250,
			url: 'select-data.json',
			data: function (term, page) {
            	return { q: term };
        	},
			results: function (data, page) {
	            return { results: data.items };
        	},
    		cache: true
		},
		initSelection: function () {}
	} );

} )( jQuery );